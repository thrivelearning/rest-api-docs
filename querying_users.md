# Querying Users
## Single Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/users  
  

**Example Query:** https://public.api.learn.link/rest/v1/users/{USERID}  
  

**Successful Response:**  HTTP Status Code 200  
  

**Example Response Body:**  
```
{
  "id": "0000000000000000000000000",
    "status": "active",
    "ref": "example_person_0000000000000000000000000",
    "firstName": "E",
    "lastName": "P",
    "email": "example.person@companyname.com",
    "role": "learner",
    "positions": [
        {
            "id": "0000000000000000000000000",
            "manager": null,
            "ouId": null,
            "isActive": true,
            "title": null,
            "startDate": "2019-03-20",
            "endDate": null,
            "createdAt": "2019-03-20T11:09:44.101Z",
            "updatedAt": null
        }
    ],
    "teamMembers": [],
    "directReports": [],
    "deleted": false,
    "compliance": 0,
    "level": 0,
    "firstLogin": null,
    "lastLogin": null,
    "tags": {
        "skills": []
    },
    "usersFollowing": [],
    "followers": [],
    "tagsFollowing": [],
    "createdAt": "2019-03-20T11:09:43.944Z",
    "updatedAt": "2020-02-20T03:56:50.553Z",
    "hasPicture": false,
    "timeZone": null,
    "summary": null,
    "relevancy": null,
    "rank": {
        "currentCategory": 0,
        "currentLevel": 1
    },
    "agreedTerms": null,
    "onboarded": null,
    "additionalFields": null,
    "languageCode": null,
    "audiences": [
        {
            "id": "5e7c8b3e252cc600102d9bf7"
        }
    ]

}
```

## Multiple Records

**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/users  
  

**Example Query:** https://public.api.learn.link/rest/v1/users?page=1&perPage=20   
  

**Successful Response:**  HTTP Status Code 200  
  

**Example Response Body:**  

```
{
    "pagination": {
        "totalResults": 309,
        "totalPages": 16,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "id": "00000000000000000000",
            "status": "active",
            "ref": "admin",
            "firstName": "Example",
            "lastName": "Person",
            "email": "example.person@companyname.com",
            "role": "administrator",
            "positions": [],
            "teamMembers": [],
            "directReports": [
                {
                    "id": "00000000000000000000"
                },
            "deleted": false,
            "compliance": 0,
            "level": 0,
            "firstLogin": "2019-01-11T10:18:26.626Z",
            "lastLogin": "2020-02-20T11:24:46.846Z",
            "tags": {
                "skills": [
                    "customer success",
                    "lxp",
                    "onboarding",
                    "faq",
                    "learning design"
                ]
            },
            "usersFollowing": [
                "00000000000000000000",
            ],
            "followers": [
                {
                    "id": "00000000000000000000"
                }
            ],
            "tagsFollowing": [
                "social learning",
                "tips",
                "content creation",
                "learning and development"
            ],
            "createdAt": "2020-02-20T12:57:05.976Z",
            "updatedAt": "2020-02-20T03:23:05.585Z",
            "hasPicture": true,
            "timeZone": null,
            "summary": "I'm a new person! Welcome to my profile!",
            "relevancy": null,
            "rank": {
                "currentCategory": 4,
                "currentLevel": 5
            },
            "agreedTerms": null,
            "onboarded": true,
            "additionalFields": null,
            "languageCode": "en-gb",
            "audiences": [
                {
                    "id": "5e7c8b3e252cc600102d9bf7"
                }
            ]
},
{
  "id": "00000000000000000000",
    "status": "active",
    "ref": "example_person_00000",
    "firstName": "E",
    "lastName": "P",
    "email": "example.person@gmail.com",
    "role": "learner",
    "positions": [
        {
            "id": "0000000000000000000000000",
            "manager": null,
            "ouId": null,
            "isActive": true,
            "title": null,
            "startDate": "2019-03-20",
            "endDate": null,
            "createdAt": "2019-03-20T11:09:44.101Z",
            "updatedAt": null
        }
    ],
    "teamMembers": [],
    "directReports": [],
    "deleted": false,
    "compliance": 0,
    "level": 0,
    "firstLogin": null,
    "lastLogin": null,
    "tags": {
        "skills": []
    },
    "usersFollowing": [],
    "followers": [],
    "tagsFollowing": [],
    "createdAt": "2019-03-20T11:09:43.944Z",
    "updatedAt": "2020-02-20T03:56:50.553Z",
    "hasPicture": false,
    "timeZone": null,
    "summary": null,
    "relevancy": null,
    "rank": {
        "currentCategory": 0,
        "currentLevel": 1
    },
    "agreedTerms": null,
    "onboarded": null,
    "additionalFields": null,
    "languageCode": null,
            "audiences": [
                {
                    "id": "5e7c8b3e252cc600102d9bf7"
                }
            ]
}

```

## User Schema Definitions
  

| Level 1  |  Level 2 |  Type |  Type |  Sample Data | Comments  |
|---|---|---|---|---|---|
|  Additional Fields | City  | Varchar  | 64KB  |  Nottingham |   |
| Additional Fields  | Country  | Varchar  | 64KB  | UK  |   |
| Additional Fields  | Level  | Varchar  |  64KB |   |   |
|  Additional Fields |  Location | Varchar  | 64KB  | Nottingham  |   |
|  Agreed Terms |   |  Datetime |   |  null | nullable timestamp, recording whether the user has accepted the terms and conditions, and if they have, when they did so.  |
|  Compliance |   |   |   |   |   |
|  Created At |   | Datetime  |   | 2019-03-13T16:15:24.000  |   |
| Deleted  |   | Boolean  |   | FALSE  |   |
|  Direct Reports | Id  |  Array of MongoDB Object IDs |  Each 24 chars |  5e68e76e75bfed00101d021d |  This will mirror information shared from Azure AD |
|  Email |   | Varchar  |  250 chars | 5c892c9c4693e9643e7d90dc@example.com  |   |
|  First Login |   |  Datetime |   | 2019-03-13T16:15:36.886  |   |
| First Name  |   | Varchar  |  250 chars | Georgia  |   |
| Followers  |   | Array of MongoDB Object IDs  | Each 24 chars  | "id": "5ccc3fdf4e9edd0010d6e67e"  |   Array of user ids for people following this user |
|  Has Picture |   | Boolean  |   | TRUE  |   |
| Id  |   | ObjectId  | 24 chars  | 5c892c9c4693e9643e7d90dc  |   |
| Language Code  |   | Varchar  | 5 chars  | en-gb  |   |
|  Last Login |   |  Datetime |   | 2020-05-04T17:14:08.271  |   |
| Last Name  |   | Varchar  |  250 chars | Sanders  |   |
|  Level |   |   |   |   |   |
|  Onboarded |  |  Boolean |   | TRUE  |   |
|  Positions | Created At  | Datetime  |   |  2019-09-17T11:02:12.692 |   |
| Positions  | End Date  | Datetime  |   |  null |   |
|  Positions | Id  | ObjectId  |  24 chars |  5d80bd34d1d98d00103389ee |   |
| Positions  | Is Active  | Varchar  |   | TRUE  |   |
| Positions  | Manager  | ObjectId  | 24 chars  |  5d80bd34d1d98d00103389ee |   |
| Positions  | OuId  |  ObjectId | 24 chars  |  5cd44f6e35c1fa33fc8a7879 | A reference to the organisational unit of this users position. Only used in some implementations  |
|  Positions | Start Date  | Datetime  |   |  2019-01-01T00:00:00.000 |   |
|  Positions | Title  | Varchar  | 250 chars|  Consultant CSM |
|  Positions | Updated At  |  Datetime |   |  2019-01-01T00:00:00.000 |   |
| Rank  | Current Category  | Integer  |   | 4  |  No larger than 10 |
|  Rank | Current Level  | Integer  |   | 5  |  No larger than 10 |
| Ref  |   |  Varchar | 250 chars  | Georgia Avery  | The unique identifier of the user, will be defined by Azure AD  |
|  Relevancy |   |   |   |  null |   |
|  Role |   | Varchar  |  24 chars | administrator  | Defines the users role at the system level, currently only used to identify administrators  |
| Status  |   |  Varchar |  24 chars | active  |   |
| Summary  |   | Varchar    | 64KB | Hello! I'm Kristina and I am the Area Sales Manager for Germany and Switzerland. I stared in the Education Team September 2018 and I Love passing on my knowledge to others.  | Freetext  |
|  Tags |  Skills |  Array of Strings | Each up to 250 chars  |  "Education, Management, Training, Product & Brand" |  |
|  Tags Following |   | Array of Strings  |  Each up to 250 chars |  "Customer Service, Stored, Retinoids, Hylamid" |  |
| Team Members |   |  Array of MongoDB Object IDs |  Each 24 chars | 5c892c9c4693e9643e7d90dc  | This will mirror information shared from Azure AD  |
| Time Zone  |   | Varchar  | 250 chars  |  Europe/Berlin |  As per tz database |
| Updated At  |   | Datetime  |   |  2020-08-16T18:26:55.377 |   |
|  Users Following |   |  Array of MongoDB Object IDs | Each 24 chars  |  5c892c9c4693e9643e7d90dc |   |