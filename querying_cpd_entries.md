# Querying  CPD Entries
## Single Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdEntries  
  

**Example Query:** https://public.api.learn.link/rest/v1/cpdEntries/{ID}  
  

**Successful Response:** HTTP Status Code 200  
  

**Example Response Body:**
```
        {
            "logEntryId": "000000000000000000000000",
            "userId": "000000000000000000000000",
            "activity": {
                "type": "file",
                "name": "International Space Station after undocking of STS 132"
            },
            "category": {
                "categoryId": "000000000000000000000000",
                "name": "TECHNOLOGY"
            },
            "entryDate": "2020-09-25T19:34:23.133Z",
            "durationMinutes": 15,
            "description": null,
            "isVerified": true
        }
```

## Multiple Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdEntries  
  

**Example Query:** https://public.api.learn.link/rest/v1/cpdEntries?page=1&perPage=20  
  

**Successful Response:** HTTP Status Code 200   
  

**Example Response Body:**  
```
{
    "pagination": {
        "totalResults": 48,
        "totalPages": 3,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "logEntryId": "000000000000000000000000",
            "userId": "000000000000000000000000",
            "activity": {
                "type": "file",
                "name": "International Space Station after undocking of STS 132"
            },
            "category": {
                "categoryId": "000000000000000000000000",
                "name": "TECHNOLOGY"
            },
            "entryDate": "2020-09-25T19:34:23.133Z",
            "durationMinutes": 15,
            "description": null,
            "isVerified": true
        },
        {
            "logEntryId": "000000000000000000000000",
            "userId": "000000000000000000000000",
            "activity": {
                "type": "file",
                "name": "International Space Station after undocking of STS 132"
            },
            "category": {
                "categoryId": "000000000000000000000000",
                "name": "TECHNOLOGY"
            },
            "entryDate": "2020-09-20T14:21:22.222Z",
            "durationMinutes": 15,
            "description": null,
            "isVerified": true
        }
```
## Date Specific Requests
The cpdEntries endpoint allows for entry date arguments to be added to the request url in order to retrieve cpd entry data from and to a specific date and time. The entryDateFrom and entryDateTo arguments should have the format YYYY-MM-DD hh:mm:ss
For example:
```
GET https://public.api.learn.link/rest/v1/cpdEntries?page=1&perPage=20&entryDateFrom=2020-09-25&entryDateTo=2020-11-1
```

## CPD Entries Schema Definitions

|  Level 1 |  Level 2 | Type  |  Size |  Sample Data |  Comments |
|---|---|---|---|---|---|
|  logEntryId |   |  Object ID |  24 chars |  5c8936159ec2d00010cdd334 | Unique ID for this activity record  |
| userID  |   | Object ID  |  24 chars | 5c8936159ec2d00010cdd334  | User ID for the user who triggered this activity record  |
|  activity | type  |  Varchar | 250 chars  |  file |  The type of activity being logged |
|  activity | name  | Varchar  |  250 chars |  Relative Time Dilation - Practical Guide | The name of the activity logged  |
| category  |  categoriyId | Object ID  | 24 chars  | 5c8936159ec2d00010cdd334  |  unique identifier for the CPD category |
| category  | name  | Varchar  | 250 chars  |  "Personal Reading" |  Name of the category of CPD activity against this was logged |
| durationMinutes  |   |  integer |   | 15  | Minutes logged as CPD from this activity  |
|  description |   | Varchar  | 64KB  |  "I realised we could be more productive if travelling close to the speed of light." | summary or reflective statement  |
| isVerified  |   | Boolean  |   |  TRUE |  Whether or not this activity was determined by user submission or generated based upon verified activity in the system |

