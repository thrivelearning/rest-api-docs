# Querying CPD Categories
## Single Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdCategories  
  

**Example Query:** https://public.api.learn.link/rest/v1/cpdCategories/{ID}  
  

**Successful Response:**  HTTP Status Code 200 
  
 
**Example Response Body:**  
```
  {
    "categoryId": "000000000000000000000000",
    "name": "ANIMALS"
  }
```
## Multiple Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdCategories  
  

**Example Query:** https://public.api.learn.link/rest/v1/cpdCategories?page=1&perPage=20  
  

**Successful Response:**  HTTP Status Code 200  
  

**Example Response Body:**  
```
{
    "pagination": {
        "totalResults": 27,
        "totalPages": 2,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "categoryId": "000000000000000000000000",
            "name": "ANIMALS"
        },
        {
            "categoryId": "000000000000000000000000",
            "name": "BOOKS"
        },
        {
            "categoryId": "000000000000000000000000",
            "name": "CLIENTS"
        },
        {
            "categoryId": "000000000000000000000000",
            "name": "COFFEE"
        },
        {
            "categoryId": "000000000000000000000000",
            "name": "COOKING"
        },
}
```
## CPD Category Schema Definitions

|  Level 1 |  Level 2 | Type  |  Size | Sample Data  | Comments  |
|---|---|---|---|---|---|
| categoryId  |   |  Object ID |  24 chars | 5c8936159ec2d00010cdd334 | Unique ID for this category record  |
| name  |   | Varchar  | 250 chars |  "Personal Reading" | Name of the category of CPD activity against this was logged  |
