# Querying Content
## Single Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/contents/:id
  

**Example Query:** https://public.api.learn.link/rest/v1/contents/0000000000000000000000000
  

**Successful Response:** HTTP Status Code 200  
  

**Example Response Body:**  
```
{
 “id": "0000000000000000000000000",
    "title": "Thrive Customer Journey",
    "description": "<p>This diagram highlights the key things you may experience on your journey as a customer of Thrive's LXP</p>",
    "tags": [
        "customer journey",
        "faq",
        "customer success",
        "lxp"
    ],
    "type": "file",
    "createdAt": "2019-02-18T17:42:11.708Z",
    "updatedAt": "2020-02-20T13:16:22.840Z",
    "author": "5c69be0e3a38412f022b4719",
    "isOfficial": trued
}
```
## Multiple Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/contents  
  

**Example Query:** https://public.api.learn.link/rest/v1/contents?page=1&perPage=20
  
  
**Successful Response:** HTTP Status Code 200  
  

**Example Response Body:**  
```
{
    "pagination": {
        "totalResults": 745,
        "totalPages": 38,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "id": "00000000000000000000",
            "title": "Thrive Customer Journey",
            "description": "<p>This diagram highlights the key things you may experience on your journey as a customer of Thrive's LXP</p>",
            "tags": [
                "customer journey",
                "faq",
                "customer success",
                "lxp"
            ],
            "type": "file",
            "createdAt": "2019-02-18T17:42:11.708Z",
            "updatedAt": "2020-02-19T21:06:02.574Z",
            "author": "5c69be0e3a38412f022b4719",
            "isOfficial": true
        },
        {
            "id": "00000000000000000000",
            "title": "Thrive on-boarding journey",
            "description": "<p>This infographic will talk you through the key things you will experience during the Thrive on boarding process</p>",
            "tags": [
                "customer journey",
                "faq",
                "customer success",
                "lxp"
            ],
            "type": "file",
            "createdAt": "2019-02-19T16:19:24.399Z",
            "updatedAt": "2020-02-19T21:06:35.384Z",
            "author": "00000000000000000000",
            "isOfficial": true
        },
}
```

## Content Schema Definitions
  
| Level 1  |  Level 2 | Type  |  Size |  Sample Data |  Comments |
|---|---|---|---|---|---|
|  Id|   |  Object ID | 24 chars  | 5c8936159ec2d00010cdd334  |  unique identifier for the tag |
| Author  |   |  Object ID |  24 chars | 5c8936159ec2d00010cdd334  |  User who authored this field. Nullable for autocurated content. |
| Created At  |   |  Datetime |   |  2019-03-13T16:15:24.000 |   |
|  Updated At |   | Datetime  |   | 2019-03-13T16:15:24.000  |   |
| Description  |   | Varchar  | 64KB  |  This content looked really interesting and relevant to recent conversations in the development team |   |
|  Is Official |   |  Bool |   |  TRUE | Whether or not this content has been recognised for its importance or validity by the customer organisation  |
|  Tags |   | Array of Strings  | 255 chars each  | "exciting", "video", "learning"  |  Unique Ids of users who are skilled in this tag |
| Title  |   | Varchar  | 255 chars  | An amazing video  |   |
|  Type |   | Varchar  | 24 chars  |  video | The kind of artefact associated with this content record  |
