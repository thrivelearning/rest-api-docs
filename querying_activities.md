# Querying Activities
## Single Records
**Method:** GET  
  
**Base URL:** https://public.api.learn.link/rest/v1/activity/:id  
  

**Example Query:** https://public.api.learn.link/rest/v1/activity/abcdend4djf4hfnshfnf64jh  
  

**Successful Response:** HTTP Status Code 200 

   
**Example Response Body:**
  
```
{
    "id": “0000000000000000000000000",
    "user": "0000000000000000000000000",
    "type": "viewed",
    "date": "2020-02-12T14:58:23.621Z",
    "contextId": "0000000000000000000000000",
    "contextType": "content",
    "data": { 
        “source”: {“source”:”feed”},
        “duration”: “3600”
    },
    "with": null
}
```
## Multiple Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/activities  
  

**Example Query:** https://public.api.learn.link/rest/v1/activities?page=1&perPage=20  
  

**Successful Response:** HTTP Status Code 200  
  

**Example Response Body:**   
```
{
    "pagination": {
        "totalResults": 34223,
        "totalPages": 1720,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "id": “0000000000000000000000000",
            "user": "0000000000000000000000000",
            "type": "viewed",
            "date": "2020-02-12T14:58:23.621Z",
            "contextId": "0000000000000000000000000",
            "contextType": "content",
            “data”: {}
            "with": null
        }, 
        ...
    ]
}
```

## Data Specific Requests
The activities endpoint allows for multiple arguments to be added to the request url in order to retrieve activity data for specific conditions. These are: 

- actions (activity types)
- contentIds 
- contentType
- timestampFrom
- timestampTo

The `actions` and `contentIds` arguments can include multiple values by separating them with commas. 

The `timestampFrom` and `timestampTo` arguments should have the format YYYY-MM-DD hh:mm:ss

For example:
```
GET https://public.api.learn.link/rest/v1/activities?timestampTo=2020-10-01&actions=viewed,completed
```
### Activity Contexts
Because activities can occur “on” different things, the context fields give information about what the activity was related to. For example, if you wanted to track all actions that occurred on an item of content, you would need to filter the data to the contextType of “content” and the contextId of your item’s ID.

### Activity Types
The Activity system is based around the concepts of xAPI and uses verbs to identify the type of user activity being logged. The endpoint exposes these under the “type” attribute, and this will include activities such as:

- viewed
- completed
- passed
- pinned
- unpinned
- liked
- unliked
- commented
- commentdeleted
- shared
- progressed
- registered
- followed
- unfollowed
- started
- resumed
- searched
- found
- loggedin
- loggedout

### Activity Data

This is an unstructured JSON object with variable content depending on the type of activity being logged.
For example, a viewed statement will list the duration of the view and the source of the view, whereas a found statement will detail the keyword of the search and the number of results on the search.

## Activity Schema Definitions 

| Level 1  | Level 2  | Type  |  Size | Sample Data  | Comments  |
|---|---|---|---|---|---|
| Id  |   |  Object ID |  24 chars | 5c8936159ec2d00010cdd334  | Unique ID for this activity record  |
|  Context Type |   |  Varchar |  24 chars | content  |  What this activity was in relation to |
|  Context Id |   | Object ID |  24 chars | 5c8936159ec2d00010cdd334  |  unique identifier for the tag |
|  Context Data |   |  JSON |   | "{ ""search"": ""development videos"", ""searchResults"": ""20""}"  |   |
|  Date |   |  Datetime|   | 2019-03-13T16:15:24.000  |   |
|  Type |   | Varchar  | 255 chars  | found  |  "The activity being tracked: Examples include: viewed, completed, passed, pinned, unpinned, liked, unliked commented, commentdeleted shared, progressed, registered, followed, unfollowed, started, resumed searched, found, loggedin, loggedout" |
| User  |   | Object ID  |  24 chars |  5c8936159ec2d00010cdd334 | User ID for the user who triggered this activity record  |


