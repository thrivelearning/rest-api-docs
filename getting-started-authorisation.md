# Getting Started and Authorisation

## Table Of Contents

1. [Getting Started with the API](#markdown-header-getting-started-with-the-api)
2. [Pagination](#markdown-header-pagination)
3. [Admin Bearer Token and Correlation ID](#markdown-header-getting-started)
4. [API Keys](#markdown-header-getting-an-api-key)
5. [Thrive API Rate Limiting](#markdown-header-thrive-api-rate-limiting)
6. [Authorisation](#markdown-header-authorisation)

## Getting Started with the API
The Thrive Learning API allows you to manage the data within the Thrive platform using HTTP requests. Our APIs are often used to add/update user records and extract data from Thrive.

To use the Thrive API, you must authenticate. Thrive uses a Basic Auth username (tenant_id) and password (API_key).

To get started with the Thrive Learning API, you must obtain your basic auth credentials using Admin Bearer Token and Correlation ID to request an API Key (access token) using GraphQL mutation and invoke the API methods using HTTP requests.

To generate your credentials please see our API authorisation guide here on Thrive Tribe: [Thrive Learner App](https://thrivetribe.learn.link/content/65c0e1b8fe5df3d9d5b9df67)

Once you have obtained your tenant ID and generated your API key as shown in our API Authorisation Steps guide. You will need to use the tenant ID as the username and API key for the password using basic Auth.

For security reasons such as the bearer token being high-jacked, the lifespan is limited to 60 minutes. 

Using our GraphQL mutation API, you generate an API key that has a life span of 100 years.

Using an API client such as Postman or Insomnia to make an HTTP request. The base URL you use to make the request depends on the environment (staging or production).

Use the following base URLs to POST your request to generate API keys:

| Data Center | Environment | GraphQL API POST URL                         |
|-------------|-------------|----------------------------------------------|
| EU          | Staging     | https://tenant.api.learnstaging.link/tenants |
| EU          | Production  | https://tenant.api.learn.link/tenants        |

Use the following base URLs when submitting requests:

| Data Center | API Base URL                   |
|-------------|--------------------------------|
| EU          | https://user.api.learn.link/   |
| EU          | https://public.api.learn.link/ |

## Pagination
THRIVE offers 5 endpoints which return a paginated data type.
For Example:
```
GET https://public.api.learn.link/rest/v1/users?page=1&perPage=20
```
When a request is made to the API, a summary of the paginated results will be returned in the response body. 

Example:
```
"pagination": {
    "totalResults": 34223,
    "totalPages": 172,
    "page": 1,
    "perPage": 20
},
```

You can alter the amount of data being requested by changing the **‘page=1&perPage=20’** in the request url. 

For example requesting:
```
GET https://public.api.learn.link/rest/v1/users?page=5&perPage=200
```
...would return all results from page 5 and 200 results per page.
```
"pagination": {
      "totalResults": 34223,
      "totalPages": 172,
      "page": 5,
      "perPage": 200
},
```
To avoid session timeouts, we advise that requests are kept to retrieve under 200 records perPage.


## Getting Started

### Acquiring Your Admin Bearer Token and Correlation ID

In order to obtain your api key using the instruction below, you will need a valid administrator bearer token and correlation-id.

Where possible, we advise that you use Chrome to obtain these details.

Login to the LXP as an admin user.

Once logged in, press **F12** to open a new **Dev Tools** window.

![DevToolsNetwork](images/DevToolsNetwork.png)

Navigate to a new page, such as the **ME** page.

From within Chrome Dev Tools, select **NETWORK** from the top menu.

Select any of the service requests from the left-hand pane of the results shown. We suggest **CONTENT**.

![DevToolsContent](images/DevToolsContent.png)

This will open a right-hand pane for the selected request.

In the right-hand pane, select **HEADER** in the sub-menu.

Scroll down to the **REQUEST HEADERS** details.

Your bearer token and correlation-id will be listed here under **AUTHORIZATION** and **CORRELATION-ID**. 

![DevToolsToken](images/DevToolsToken.png)

For the bearer token, copy the entire value including the word **“Bearer”**

These 2 values can then be used as authorisation to obtain your api key using the mutation query outlined below.

### Getting an API Key

**Method:** POST

**Authentication:** Administrator Bearer Token, Content-Type and correlation-ID which will need to be added to the request header.

**Request Body:**

```
https://tenant.api.learn.link/tenants?query=mutation {
  generateApiKey (
    tokenType: "permanent", services: ["api-service"]
  ) {
      validUntil,
      apiKey
    }
}
```
**Successful Example Response:**
```
{
  "data": {
    "generateApiKey": {
      "validUntil": "2120-02-11T11:11:00.000Z",
        "apiKey": "00000000000000000000000000000"
      }
  }
}
```
**Example Request using Postman API:**

**Body:**

![DevToolsBody](images/DevToolsBody.png)

**Headers:**

![DevToolsHeaders](images/image2.png)

## Thrive API Rate Limiting

### Understanding our API Limit: 1,000 Requests per Minute

In today’s fast-paced digital environment, APIs (Application Programming Interfaces) play a crucial role in enabling seamless integration and communication between different software systems. To ensure optimal performance and reliability, we have implemented a recommended API rate limit of 1,000 requests per minute. This guide will explain what this limit means, why it’s important, and how you can work within it to make the most of our API services.

### What is an API Rate Limit?

An API rate limit is a restriction placed on the number of API calls a client can make to a server within a specific time period. This is done to prevent abuse, ensure fair usage, and maintain the overall performance and stability of the API service.

### Why is There a Rate Limit?

1. **Preventing overload:** High volumes of requests can overwhelm servers, leading to slow response times or even downtime. By capping the number of requests, we protect the infrastructure from being overloaded.
2. **Fair usage:** Rate limiting ensures that all users have equal access to the API and that no single client can monopolize the resources.
3. **Performance and reliability:** Consistent and predictable performance is crucial. Rate limits help maintain a high quality of service for all users.

### Our Rate Limit: 1,000 Requests per Minute

To strike a balance between performance and usability, we recommend a rate limit of 1,000 requests per minute. This allows for robust interaction with our API while safeguarding against potential performance issues.

### How to Work Within the Rate Limit

Here are some best practices to ensure you stay within the recommended rate limit while efficiently utilizing our API:

1. **Batch your requests:** Where possible, group multiple operations into a single request. For example, use bulk endpoints that handle multiple records at once.
2. **Optimize your queries:** Ensure your API requests are as efficient as possible. Retrieve only the data you need by using appropriate filters and parameters.
3. **Implement caching:** Cache API responses where appropriate to reduce the need for repetitive calls. This is especially useful for data that doesn’t change frequently.
4. **Rate limiting logic:** Implement client-side logic to monitor and respect the rate limit. If your application is approaching the limit, it can queue requests or implement exponential backoff to prevent exceeding the limit.
5. **Monitor usage:** Regularly monitor your API usage to ensure compliance with the rate limit. Use the provided response headers that indicate your current rate limit status.

### Conclusion

By adhering to the recommended rate limit of 1,000 requests per minute, you can ensure smooth and efficient use of our API services. Implementing best practices and handling rate limits gracefully will help maintain the performance and reliability of your applications while respecting the shared resources of the API.

## Authorisation

**All header requests are sent in Basic Auth consisting of your accounts Username (Tenant ID) and Password (API Key).**

**Authorisation type:** Basic Auth

**Username:** Your website Tenant ID (eu-west-000000 please contact support for your tenant).

**Password:** Your generated API Key

![DevToolsAuth](images/DevToolsAuth.png)