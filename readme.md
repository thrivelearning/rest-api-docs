# Rest Web Service
## Accessing Data Via Rest Endpoints

### Introduction
THRIVE offers REST API endpoints for paginated and Individual data type requests.

#### Datasets
The service offers endpoints which allow the retrieval of single records or multiple records within the following datasets of the system:

- Content - including files, videos, links, quizzes etc
- Users - all user profiles on the system
- Tags - tags, including their usage by users and content
- Activities - records of all user actions on the system
- CPD - Continuous professional development user entries, user summaries and categories on your LXP.
- Compliance - Retrieve the assignment details and enrolment statuses for assigned learning.
- Completions - Access completions of content.

This documentation will summarise how to access individual records as well as querying for multiple records.

Further information regarding Pagination, Admin Bearer Tokens, Correlation IDs, and Authorisation can be found on the page linked below.

- [Getting Started/Authorisation](getting-started-authorisation.md)

You can send a request to any of the following endpoints within the Available Endpoint pages listed below.

- [Querying Users](querying_users.md)
- [Querying Tags](querying_tags.md)
- [Querying Content](querying_content.md)
- [Querying Activities](querying_activities.md)
- [Querying CPD Entries](querying_cpd_entries.md)
- [Querying CPD Categories](querying_cpd_categories.md)
- [Querying CPD Requirements](querying_cpd_requirements.md)
- [Querying CPD User Summary](querying_cpd_user_summary.md)
- [Querying Compliance and Completions API](https://api-docs.learndev.link/openapi)

