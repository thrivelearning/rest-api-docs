# Querying Tags
## Single Records

**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/tag  
  

**Example Query:** https://public.api.learn.link/rest/v1/tag/{TAGID}  
  

**Successful Response:** HTTP Status Code 200  
  

**Example Response Body:**
```
{
          "id": "00000000000000000000",
          "tag": "customer journey",
          "contents": [
		  "00000000000000000000",
          ],
          "interests": [
     	  "00000000000000000000",
		  "00000000000000000000",
          ],
          "skills": [
		  "00000000000000000000",
		  "00000000000000000000",
          ],
          "campaigns": []
}
```
## Multiple Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/tag  
  

**Example Query:** https://public.api.learn.link/rest/v1/tags?page=1&perPage=20  
  

**Successful Response:** HTTP Status Code 200  
  

**Example Response Body:**
```
{
    "pagination": {
        "totalResults": 298,
        "totalPages": 15,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "id": "00000000000000000000",
            "tag": "customer journey",
            "contents": [
		"00000000000000000000",
            ],
            "interests": [],
            "skills": [
		"00000000000000000000",
            ],
            "campaigns": []
        },
        {
            "id": "00000000000000000000",
            "tag": "customer success",
            "contents": [
           	"00000000000000000000",
            ],
            "interests": [],
            ],
            "skills": [
       	 "00000000000000000000",
		 "00000000000000000000",	
           ],
            "campaigns": []
        }
}
```

## Tag Schema Definitions

| Level 1  | Level 2  | Type  | Size  | Sample Data  | Comments  |
|---|---|---|---|---|---|
| Tag Name  |   |  Varchar | 250 chars  | "Finisher, The Ordinary, Etc…"  | One per Tag Id  |
|  Campaigns |   | Array of MongoDB Object IDs  | Each 24 chars  | 5cd944e040ab140010c1e304  | Unique Ids of campaigns using this tag  |
| Contents  |   | Array of MongoDB Object IDs  | Each 24 chars  | 5cd944e040ab140010c1e304  | Unique Ids of contents using this tag  |
| Id  |   | Object ID  | 24 chars  | 5c8936159ec2d00010cdd334  | unique identifier for the tag  |
|  Interests |   | Array of MongoDB Object IDs  | Each 24 chars  | 5f089d59a5133919cf3c9eb3  | Unique Ids of users who are interested in this tag  |
| Skills  |   | Array of MongoDB Object IDs  | Each 24 chars  | 5d7a5382487e9500144eae65  | Unique Ids of users who are skilled in this tag  |

# Tag Management API

Use the [Tag Management API](https://api-docs.learndev.link/tags/) for the following:

- **Add Existing Tag(s) to Learner**
- **Remove Existing Tag(s) from Learner**
- **Update Level and/or Target Level for Skill(s)**
- **Get Available Skill Levels**