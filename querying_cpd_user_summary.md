# Querying CPD User Summary
## Single Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdUserLogSummaries 
  
 
**Example Query:** https://public.api.learn.link/rest/v1/cpdUserLogSummaries?userIds=5c9de5c64a6b1c37a58e393c,5cadda57f4a0f40010af03de&page=1&perPage=5&entryDateFrom=2020-11-17T09:50:00&entryDateTo=2021-11-17T09:51:00  
  

**Successful Response:**  HTTP Status Code 200  
  

**Example Response Body:**  
```
{
    "pagination": {
        "totalResults": 2,
        "totalPages": 1,
        "page": 1,
        "perPage": 100
    },
    "results": [
        {
            "userId": "5c9de5c64a6b1c37a58e393c",
            "durationMinutes": 360
        },
        {
            "userId": "5cadda57f4a0f40010af03de",
            "durationMinutes": 620
        }
    ]
}
```
## Parameters
- userIds - optional - list of userIds to query for - if not supplied all users that match the rest of the query are returned
- page - optional - page to return (defaults to 1 when not supplied)
- perPage - optional - number of users per page to return (defaults to 100 when not supplied)
- entryDateFrom - mandatory - what date to begin querying for
- entryDateTo - mandatory - what date to begin querying to

## Response
For each user supplied (or all users if not userIds supplied) who have logged CPD activity during the given time period, a sum of all minutes recorded is returned.
