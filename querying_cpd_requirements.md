# Querying CPD Requirements
## Single Records
**Method:** GET   
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdRequirementSummaries 
  
 
**Example Query:** https://public.api.learn.link/rest/v1/cpdRequirementSummaries/{ID}  
  

**Successful Response:**  HTTP Status Code 200  
  

**Example Response Body:**
```
  {
    "audienceRequirementId": "000000000000000000000000",
    "audienceId": "000000000000000000000000",
    "requiredMinutes": 660,
    "createdAt": "2020-05-27T10:44:05.687Z",
    "updatedAt": "2020-08-05T09:37:47.824Z"
  }
```

## Multiple Records
**Method:** GET  
  

**Base URL:** https://public.api.learn.link/rest/v1/cpdRequirementSummaries 
  
 
**Example Query:** https://public.api.learn.link/rest/v1/cpdRequirementSummaries?page=1&perPage=20  
  

**Successful Response:**  HTTP Status Code 200  
  

**Example Response Body:** 
```
{
    "pagination": {
        "totalResults": 2,
        "totalPages": 1,
        "page": 1,
        "perPage": 20
    },
    "results": [
        {
            "audienceRequirementId": "000000000000000000000000",
            "audienceId": "000000000000000000000000",
            "requiredMinutes": 660,
            "createdAt": "2020-05-27T10:44:05.687Z",
            "updatedAt": "2020-08-05T09:37:47.824Z"
        },
        {
            "audienceRequirementId": "000000000000000000000000",
            "audienceId": "000000000000000000000000",
            "requiredMinutes": 1200,
            "createdAt": "2020-06-11T21:03:55.798Z",
            "updatedAt": null
        }
}
```

